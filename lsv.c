#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<dirent.h>
#include<string.h>

#include <sys/stat.h> 
#include<sys/types.h> 
#include<grp.h>
#include<pwd.h>
#include<errno.h>
#include<time.h>
void printdir(char*); 
void printdir1 (char * dir,int argv);
char* getgidname(int gid);
char* getuidname(int uid);
char* gettime(long mtime);
char* getmode(unsigned int mode);

int main(int argc,char* argv[])
{   
	if(argc==1) 
	{
		printdir("."); 	
	}
	int i=0;
	while(++i < argc)
   	{
		if(strcmp(argv[1],"-li")==0)
		{
			if(argc==2)
				printdir1(".",argc);
			else if(argc>2)
				printdir1(argv[i],argc); 	
		}
		else
		{
			printdir(argv[i]); 
		}
      	}
  return 0; 
}
int mysort(const void*x,const void* y)
{
	return strcmp(*(char* const*)x,*(char* const*)y);
} 
void printdir1 (char * dir,int  argc)
{   
	if(strcmp(dir,"-li")==0)
	{ 
		return;
	}
	if(argc>2) 
		printf("%s:\n",dir);
	struct dirent * entry;                     // dirent object returned by readdir
	DIR * dp;
	dp = opendir(dir);   
	if (dp == NULL)
	{     
		 fprintf(stderr, "Cannot open dir:%s\n",dir);     
		 return;   
	}   
	chdir(dir); 

	int count=0;
	while((entry = readdir(dp)) != NULL)         // for getting count of all names in dir
	{ 
		count++;
	}
	 seekdir(dp,SEEK_SET);                   // set reading at start
	 //printf("count is:%d \n",count);

	char** arr=(char**)malloc(sizeof(char*)*count);         // creating 2d array for storing files name
	for(int i=0;i<count;i++)
	{
		arr[i]=(char*)malloc(sizeof(char)*20);
	}

	int i=0;
	for (int i = 0; i < count; i++) 
	{
		while((entry = readdir(dp)) != NULL)           // for storing data in array by reading dir
		{  
			strcpy(arr[i],entry->d_name);
			//printf("array :%s\n",arr[i]);
		
			i++;
		}
	}

	qsort(arr,count,sizeof(char*),mysort);
	
	
		struct stat info;
		printf("\n");
		for(int i=0;i<count;i++)
		{
			if(arr[i][0]!='.')
			{   
				stat(arr[i], &info);
				//printf("\n======================= %s   =================\n",arr[i]); 
				// printf("%ld  ", info.st_ino); 
				printf("%s\t", getmode(info.st_mode));    
				printf("%ld\t", info.st_nlink);           
				printf("%s\t", getuidname(info.st_uid));            
				printf("%s\t", getgidname(info.st_gid));          
				printf("%ld\t\t", info.st_size);        
				printf("%s\t", gettime(info.st_mtime)); 
				printf("%s \n", arr[i]); 
			} 
		}
		
	
	closedir(dp);
	printf("\n"); 
} 
void printdir (char * dir)
{   
	
	struct dirent * entry;                     // dirent object returned by readdir
	DIR * dp;
	dp = opendir(dir);   
	if (dp == NULL)
	{     
		 fprintf(stderr, "Cannot open dir:%s\n",dir);     
		 return;   
	}   
	chdir(dir); 

	int count=0;
	while((entry = readdir(dp)) != NULL)         // for getting count of all names in dir
	{ 
		count++;
	}
	 seekdir(dp,SEEK_SET);                   // set reading at start
	 //printf("count is:%d \n",count);

	char** arr=(char**)malloc(sizeof(char*)*count);         // creating 2d array for storing files name
	for(int i=0;i<count;i++)
	{
		arr[i]=(char*)malloc(sizeof(char)*20);
	}

	int i=0;
	for (int i = 0; i < count; i++) 
	{
		while((entry = readdir(dp)) != NULL)           // for storing data in array by reading dir
		{  
			strcpy(arr[i],entry->d_name);
			//printf("array :%s\n",arr[i]);
		
			i++;
		}
	}

	qsort(arr,count,sizeof(char*),mysort);
			
		for(int i=0;i<count;i++)
		{
			if(arr[i][0]!='.')
			{   
			 printf("%s \t",arr[i]);
			} 
		}
	printf("\n");
	closedir(dp);
} 

char* getmode(unsigned int mode)
{
	char* arr=(char*)malloc(sizeof(char)*10);  
	char buff[]="----------";
	
	//printf("%d\n",mode); 
	//printf("%s\n",buff);
	if((mode & 0000400) == 0000400) 
	{
		//printf("Owner has read permission\n");
		buff[1]='r'; 
	}
	if((mode & 0000200) == 0000200) 
	{
		//printf("Owner has write permission\n");
		buff[2]='w'; 
	}
	if((mode & 0000100) == 0000100) 
	{
		//printf("Owner has execute permission\n");
		if((mode & 0004000) == 0004000) 
		{
			buff[3]='s';
		}
		else
		{
			buff[3]='x';
		}
	}
	if((mode & 0000400) == 0000400 && (mode & 0000100) != 0000100) 
	{
		//printf("Owner has read permission\n");
		buff[3]='S'; 
	}
	if((mode & 0000040) == 0000040) 
	{
		//printf("group has read permission\n"); 
		buff[4]='r';
	}
	if((mode & 0000020) == 0000020) 
	{
		//printf("group has write permission\n");
		buff[5]='w';
	} 
	if((mode & 0000010) == 0000010) 
	{
		//printf("group has execute permission\n");
		if((mode & 0002000) == 0002000) 
		{
			buff[6]='s';
		}
		else
		{
			buff[6]='x';
		}
	}
	if((mode & 0002000) == 0002000 && (mode & 0000010) != 0000010) 
		{
			buff[6]='S';
		}
	if((mode & 0000004) == 0000004) 
	{
		//printf("other has read permission\n");
		buff[7]='r'; 
	}
	if((mode & 0000002) == 0000002) 
	{
		//printf("Other has write permission\n");
		buff[8]='w'; 
	}
	if((mode & 0000001) == 0000001) 
	{
		//printf("Other has execute permission\n");
		if((mode & 0001000) == 0001000) 
		{
			buff[9]='t';
		}
		else
		{
			buff[9]='x';
		}
	}
	if((mode & 0000001) == 0000001 && (mode & 0001000) == 0001000) 
	{
		buff[9]='T';
	}
	//printf("%s",buff);
	if((mode& 0170000) ==0010000)
	{
		//printf("pipe \n");
		buff[0]='p';
	}
	if((mode& 0170000) ==0020000)
	{
		//printf("character special file\n");
		buff[0]='c';
	}
	if((mode& 0170000) ==0040000)
	{
		//printf("directory \n");
		buff[0]='d';
	}
	if((mode& 0170000) ==0060000)
	{
		//printf("block special file\n");
		buff[0]='b';
	}
	if((mode& 0170000) ==0100000)
	{
		//printf("regular file\n");
		buff[0]='-';
	}
	if((mode& 0170000) ==0120000)
	{
		//printf("link file\n");
		buff[0]='l';
	}
	if((mode& 0170000) ==0140000)
	{
		//printf("socket file\n");
		buff[0]='s';
	}
	arr=buff;
	return arr;
}
char* gettime(long secs)
{
	
	char* time;
	time=ctime(&secs);
	char* mtime;
	int len=strlen(time);
	int c=4;
	for(int i=0;i<len-13;i++)
	{
		mtime[i]=time[c];
	 	c++;
	}
	//printf("%d",len);
	//printf("%s",mtime);
	return mtime;
}
char* getuidname(int uid)
{
 	errno = 0;   
	struct passwd * pwd = getpwuid(uid);     
	if (pwd == NULL)
	{      
		if (errno == 0)         
		printf("Record not found in passwd file.\n");     
		 else         
			perror("getpwuid failed");   
	}   
	else 
	{      
		//printf("Name of user is: %s\n",pwd->pw_name);
		return pwd->pw_name;
	}
}
char* getgidname(int gid)
{
 	struct group * grp = getgrgid(gid);     
	errno = 0;   
	if (grp == NULL)
	{      
		if (errno == 0)          
			printf("Record not found in /etc/group file.\n");     
		 else          
			perror("getgrgid failed");   
	}
	else 
	{     
		//printf("Name of group is: %s \n", grp->gr_name); 
		return grp->gr_name;
	}
}

